/*
 * Скерч для управления гиропоникой (выращивание растений без почвы)
 * Аппаратная часть: микроконтроллер Arduino Pro mini 5V с подключенными модулями: 
 * 1. 2-х канальный модуль реле;
 * 2. модуль часов реального времени, подключенная по шине I2C;
 * 3. кнопка принудительного включения полива
 * Питание осуществляется через пнижающий трансформатор переменного тока 220В->12В, выпремляется диодным выпрямителем и
 * сглавживается электролитическим конденсатором. Понижение до 5В осуществляется стабилизатором напрядения LM7805.
 * Реле ОТКРЫВАЕТСЯ при подаче низкого уровня сигнала (LOW) на управляющий пин.
 * Реле ЗАКРЫВАЕТСЯ при подаче высокого уровня сигнала (HIGH) на управляющий пин.
 */

#include <StaticThreadController.h>
#include <Thread.h>
#include <ThreadController.h>
#include <RTClib.h>
#include <CyberLib.h>

RTC_DS1307 rtc;

const long waitInterval = 600000; //Интервал между поливами
const long workInterval = 180000; //время полива полива
const int onLampTime = 7; //Час, после которого включать освещение растений
const int offLampTime = 21; //Час, после которого необходимо выключить освещение растений

bool lightOn = false; //включена ли лампа

Thread lightThread = Thread();
Thread pompThread = Thread();
Thread _lightThread = Thread();

void setup() {
  D10_Out;        //Пин D10 - помпа
  D11_Out;        //Пин D11 - свет
  D12_In;         //Пин D12 - кнопка
  D10_High;
  D11_High;
  UART_Init(57600);
  rtc.begin();
  lightThread.onRun(lightWork);
  lightThread.setInterval(900000);
  _lightThread.onRun(lightWork);
  _lightThread.setInterval(1000);
  pompThread.onRun(pompWork);
  pompThread.setInterval(1000);

}

void loop() {
  Start
  if (!lightOn)
  {
    if (lightThread.shouldRun())
      lightThread.run();
  }
  else
  {
    if (_lightThread.shouldRun())
      _lightThread.run();
  }
  if (pompThread.shouldRun())
    pompThread.run();
  End
}

void lightWork()
{
  DateTime now = rtc.now();
  int time = now.hour();
  int t2=now.minute();
  Serial.println(t2);
  if (time >= onLampTime && time < offLampTime)
  {
    D11_Low;
    lightOn=true;
    Serial.println("Лампа включена");
  }
  else
  {
    D11_High;
    lightOn=false;
    Serial.println("Лампа откл");
  }
}

void pompWork()
{
  if (D12_Read)
  {
    do
    {
      D10_Low;
    } while (D12_Read);
    D10_High;
  }
  else
  {
    static bool state;
    static unsigned long time;
    if ((millis() - time) > (state ? waitInterval : workInterval))
    {
      state = !state;
      digitalWrite(10, state);
      time = millis();
    }
  }

}

