/*
   Скерч для управления гиропоникой (выращивание растений без почвы) в автоматическом режиме и с использованием смартфона с установленной платформой Blynk
   Аппаратная часть: микроконтроллер ESP8266 в виде модуля ESP-12 с подключенными модулями:
   1. 2-х канальный модуль реле;;
   2. кнопка принудительного включения полива
   3. датчик теммпературы/влажности
   Питание осуществляется через пнижающий трансформатор переменного тока 220В->12В, выпремляется диодным выпрямителем и
   сглавживается электролитическим конденсатором. Понижение до 3,3В осуществляется стабилизатором напрядения LM317.
   Реле ОТКРЫВАЕТСЯ при подаче низкого уровня сигнала (LOW) на управляющий пин.
   Реле ЗАКРЫВАЕТСЯ при подаче высокого уровня сигнала (HIGH) на управляющий пин.
   Данный скетч записывался в энергонезависимую память контроллера ESP8266
   V1 - V13 обозначения кнопок в конструкторе Blynk в смартфоне
*/


#include <ESP8266WiFi.h>
#include <BlynkSimpleEsp8266.h>
#include <SimpleTimer.h>
#include <SimpleDHT.h>
#include <Ticker.h>

#include <TimeLib.h>
#include <WiFiUdp.h>

//Токен авторизации (Auth Token) в приложении Blynk на смартфоне
char auth[] = "********************************";

// Настройка сети Wi-Fi..
char ssid[] = "****";
char pass[] = "*******";

SimpleDHT11 dht11;
SimpleTimer timer;
Ticker flipper;

int pinRelayLight = 5;
int pinRelayPomp = 4;
int pinButton = 14;
int pinDHT11 = 2;
bool workingPomp = false;
int telButtonPomp = 0;

byte temp = 0, hum = 0;
uint32_t periodHour = 0, periodMinute = 0, periodSecond = 0; //Время между поливами
uint32_t timeOfWork = 0; //Время работы помпы
uint32_t startLightTime = 0, stopLightTime = 0;

WidgetLED wPomp(V6); //LED работающей помпы (идет полив)
WidgetLED wLight(V8); //LED работающего света

//////////////////////////////////////////////////////////////////////
//// Синхронизация времени при соединении ///////////////////////////
/////////////////////////////////////////////////////////////////////
static const char ntpServerName[] = "time.nist.gov";
const int timeZone = 3;
WiFiUDP Udp;
unsigned int localPort = 8888;
time_t getNtpTime();
void sendNTPpacket(IPAddress &address);

BLYNK_CONNECTED()
{

  Blynk.syncAll(); //Сиинхронизация при запуске

  WiFi.localIP();
  Udp.begin(localPort);
  Udp.localPort();
  setSyncProvider(getNtpTime);
}
const int NTP_PACKET_SIZE = 48; // Время NTP находится в первых 48 байтах сообщения.
byte packetBuffer[NTP_PACKET_SIZE]; //буфер для хранения входящих и исходящих пакетов

time_t getNtpTime()
{
  IPAddress ntpServerIP; // ip-адрес NTP-сервера

  while (Udp.parsePacket() > 0) ; //отбрасывать все ранее полученные пакеты

  // получить случайный сервер из пула
  WiFi.hostByName(ntpServerName, ntpServerIP);

  sendNTPpacket(ntpServerIP);
  uint32_t beginWait = millis();
  while (millis() - beginWait < 1500) {
    int size = Udp.parsePacket();
    if (size >= NTP_PACKET_SIZE) {

      Udp.read(packetBuffer, NTP_PACKET_SIZE);  // считывание пакета в буфер
      unsigned long secsSince1900;
      // преобразовать четыре байта, начиная с местоположения 40, в unsigned long
      secsSince1900 =  (unsigned long)packetBuffer[40] << 24;
      secsSince1900 |= (unsigned long)packetBuffer[41] << 16;
      secsSince1900 |= (unsigned long)packetBuffer[42] << 8;
      secsSince1900 |= (unsigned long)packetBuffer[43];
      return secsSince1900 - 2208988800UL + timeZone * SECS_PER_HOUR;
    }
  }
  return 0; // возвратить 0 если значение времени не получено
}

// отправить NTP запрос на сервер времени по указанному адресу
void sendNTPpacket(IPAddress &address)
{
  // установить все байты в буфере в 0
  memset(packetBuffer, 0, NTP_PACKET_SIZE);
  // Инициализировать значения, необходимые для формирования NTP-запроса
  // (see URL above for details on the packets)
  packetBuffer[0] = 0b11100011;
  packetBuffer[1] = 0;     // Тип часов
  packetBuffer[2] = 6;     // Интервал опроса
  packetBuffer[3] = 0xEC;  // Точность часов
  packetBuffer[12] = 49;
  packetBuffer[13] = 0x4E;
  packetBuffer[14] = 49;
  packetBuffer[15] = 52;
  // все поля NTP заплнены и шлем запрос
  Udp.beginPacket(address, 123); //NTP-запросы на порт 123
  Udp.write(packetBuffer, NTP_PACKET_SIZE);
  Udp.endPacket();
}

//Интервал между поливами
BLYNK_WRITE(V2 /*Часы*/)
{
  periodHour = (long)param.asInt();
}
BLYNK_WRITE(V3 /*Минуры*/)
{
  periodMinute = (long)param.asInt();
}

//Время работы помпы, мин.
BLYNK_WRITE(V4)
{
  timeOfWork = (long)param.asInt();
}

BLYNK_WRITE(V7)
{
  TimeInputParam t(param);
  if (t.hasStartTime())
  {
    startLightTime = t.getStartHour() * 60 + t.getStartMinute();
    //Serial.println( startLightTime);
  }
  if (t.hasStopTime())
  {
    stopLightTime = t.getStopHour() * 60 + t.getStopMinute();
  }
}

//Принудительный полив кнопкой с телефона
BLYNK_WRITE(V13)
{
  telButtonPomp = param.asInt();
}

void ReconnectBlynk(void)
{
  if (!Blynk.connected()) {
    if (Blynk.connect()) {
      BLYNK_LOG("Reconnected");
    }
    else {
      BLYNK_LOG("Not reconnected");
    }
  }
}

void SendTempHum(void)
{
  dht11.read(pinDHT11, &temp, &hum, NULL);
  Blynk.virtualWrite(V0, temp);
  Blynk.virtualWrite(V1, hum);
}

void FirstStart()
{
  if (digitalRead(pinRelayPomp) == HIGH)
  {
    digitalWrite(pinRelayPomp, LOW);
    delay(3000);
    digitalWrite(pinRelayPomp, HIGH);
    delay(3000);
    digitalWrite(pinRelayPomp, LOW);
    delay(3000);
    digitalWrite(pinRelayPomp, HIGH);
    delay(3000);
    digitalWrite(pinRelayPomp, LOW);
    delay(3000);
    digitalWrite(pinRelayPomp, HIGH);
  }
}

//Основная функция работы
void PompProg(void)
{
  Blynk.virtualWrite(V9, String(hour()) + ":" + String(minute()) + ":" + String(second()));
  //Serial.println("telButtonPomp: "+String(telButtonPomp));

  //*************************************************//
  //**********Ключение помпы по кнопке***************//
  //*************************************************//
  if (digitalRead(pinButton) == HIGH || telButtonPomp)
  {
    FirstStart();
    digitalWrite(pinRelayPomp, LOW); //Включмть помпу
    wPomp.on();
    workingPomp = true;
  }
  else if (workingPomp)
  {
    digitalWrite(pinRelayPomp, HIGH);
    wPomp.off();
    workingPomp = false;
  }

  //*************************************************//
  //**********Ключение помпы по времени***************//
  //*************************************************//
  if (digitalRead(pinButton) == LOW && !telButtonPomp)
  {
    uint32_t _timeOfWork = timeOfWork * 60 * 1000;
    uint32_t wPeriod = (periodHour * 3600 + 60 * periodMinute) * 1000;
    static bool state;
    static uint32_t _time;
    Blynk.virtualWrite(V5, "Полив:" + String(_timeOfWork / 1000) + "c" + " Интервал:" + String(wPeriod / 1000) + "c");
    if ((millis() - _time) > (state ? _timeOfWork : wPeriod))
    {
      FirstStart();
      state = !state;
      //Serial.println(state);
      digitalWrite(pinRelayPomp, !state);
      _time = millis();
    }
    if (state)
      wPomp.on();
    else wPomp.off();
  }
}

void LightProg()
{
  unsigned long currentTime = hour() * 60 + minute();
  if (currentTime >= startLightTime && currentTime < stopLightTime)
  {
    digitalWrite(pinRelayLight, LOW); //Включить свет, замкнув реле (по LOW)
    wLight.on();
  }
  else
  {
    digitalWrite(pinRelayLight, HIGH); //Включить свет, замкнув реле (по LOW)
    wLight.off();
  }
}

void setup()
{
  pinMode(pinRelayLight, OUTPUT);
  pinMode(pinButton, INPUT);
  digitalWrite(pinRelayLight, HIGH); //размыкание реле по умолчанию
  pinMode(pinRelayPomp, OUTPUT);
  digitalWrite(pinRelayPomp, HIGH); //размыкание реле по умолчанию
  Blynk.begin(auth, ssid, pass, IPAddress(192, 168, 1, 9));
  while (Blynk.connect() == false) {}
  timer.setInterval(1000L, SendTempHum);
  timer.setInterval(1000L, PompProg);
  timer.setInterval(300000L, ReconnectBlynk);
  timer.setInterval(1000L, LightProg);
}

void loop()
{
  if (Blynk.connected()) {
    Blynk.run();
  }
  Blynk.run();
  timer.run();
}